社会的发展和科学技术的进步，互联网技术越来越受欢迎。手机也逐渐受到广大人民群众的喜爱，也逐渐进入了每个用户的使用。手机具有便利性，速度快，效率高，成本低等优点。 因此，构建符合自己要求的操作系统是非常有意义的。

本文从管理员、用户和商家的功能要求出发，微信外卖小程序中的功能模块主要是实现管理员服务端；首页、个人中心、食品类型管理、商户信息管理、外卖信息管理、用户管理、商家管理、系统管理、订单管理，商家服务端：首页、个人中心、商户信息管理、外卖信息管理、用户管理、订单管理，用户客户端：首页、商户信息、外卖信息、我的。

本微信外卖小程序系统经过认真细致的研究，精心准备和规划，最后测试成功，系统可以正常使用。分析功能调整与微信外卖小程序实现的实际需求相结合，讨论了微信开发者技术与后台结合java语言和mysql数据库开发微信外卖小程序的使用。

关键字：微信外卖小程序  微信开发者  Java技术  mysql数据库

基本可以实现软件的功能： 
1、开发实现微信外卖小程序的整个系统程序；
 
2、管理员服务端；首页、个人中心、食品类型管理、商户信息管理、外卖信息管理、用户管理、商家管理、系统管理、订单管理等。

3、商家服务端：首页、个人中心、商户信息管理、外卖信息管理、用户管理、订单管理

4、用户客户端：首页、商户信息、外卖信息、我的等相应操作；

5、基础数据管理：实现系统基本信息的添加、修改及删除等操作，并且根据需求进行交流信息的查看及回复相应操作。
